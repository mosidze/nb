variable "region" {
  description = "VPC Region"
  default = "us-east-2"
}

variable "vpc_cidr_block" {
  description = "VPC CIDR"
  default = "10.0.0.0/16"
}

variable "public_subnet1_cidr_block" {
  description = "Public Subnet 1 CIDR"
  default = "10.0.1.0/24"
}



variable "private_subnet1_cidr_block" {
  description = "Private Subnet 1 CIDR"
  default = "10.0.3.0/24"
}



variable "public_subnet1_az" {
  description = "Public Subnet 1 Availability Zone"
  default = "us-east-2a"
}




variable "private_subnet1_az" {
  description = "Private Subnet 1 Availability Zone"
  default = "us-east-2b"
}



variable "key_path" {
  description = "Public Key path"
  default = "key.pem"
}

variable "ami" {
  description = "Ubuntu18"
  default = "ami-0b9064170e32bde34"
}

variable "instance_type" {
  description = "Server Instance Type"
  default = "t2.micro"
}
