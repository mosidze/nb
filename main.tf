provider "aws" {
  region = "${var.region}"
}

resource "aws_vpc" "default" {
  cidr_block = "${var.vpc_cidr_block}"

}

resource "aws_subnet" "public-subnet1" {
  cidr_block = "${var.public_subnet1_cidr_block}"
  vpc_id = "${aws_vpc.default.id}"
  availability_zone = "${var.public_subnet1_az}"

  tags= {
    Name = "public-subnet-${var.public_subnet1_az}"
  }
}


resource "aws_subnet" "private-subnet1" {
  cidr_block = "${var.private_subnet1_cidr_block}"
  vpc_id = "${aws_vpc.default.id}"
  availability_zone = "${var.private_subnet1_az}"

  tags= {
    Name = "private-subnet-${var.private_subnet1_az}"
  }
}



resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.default.id}"

  tags = {
    Name = "Internet Gateway"
  }
}

resource "aws_route_table" "default" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags = {
    Name = "Route table for Public subnet"
  }
}

resource "aws_route_table_association" "rt-asso-public-subnet1" {
  subnet_id = "${aws_subnet.public-subnet1.id}"
  route_table_id = "${aws_route_table.default.id}"
}



resource "aws_security_group" "apsg" {
  name = "apsg"
  description = "Allow Incoming HTTP traffic"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags= {
    Name = "security-group"
  }
}


resource "aws_key_pair" "default" {
  key_name = "tierkey"
  public_key = "${file("${var.key_path}")}"
}

resource "aws_instance" "app" {
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name = "${aws_key_pair.default.id}"
  user_data = "${file("bootstrap.sh")}"
  vpc_security_group_ids = ["${aws_security_group.apsg.id}"]
  subnet_id = "${aws_subnet.public-subnet1.id}"
  associate_public_ip_address = true

  tags = {
    Name = "app-${var.public_subnet1_az}"
  }
}
